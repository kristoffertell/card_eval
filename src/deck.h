// Copyright (C) 2019 Kristoffer Tell - All Rights Reserved

#include <vector>
#include <string>
#include <atomic>
#include <mutex>
#include <thread>

#include "card.h"

class Deck
{
	public:
		Deck();
		~Deck();
		bool Init(std::vector<std::string> data);
		void SellCards();
		bool Empty();

	protected:
		void add_card(Card& card);
		unsigned get_num_cards();
		void start_updater();
		void start_seller();
		unsigned find_closest_before(Card&, Card::angle_type);
		unsigned find_closest_after(Card&, Card::angle_type);
		void update_uniq();
		void sell_least_unique();

		std::mutex m_mutex;
		std::atomic<bool> m_initialized;
		std::atomic<bool> m_needs_update;
		std::atomic<bool> m_start_selling;
		std::thread m_deck_updater;
		std::thread m_card_seller;
		std::vector<Card> m_cards;
};
