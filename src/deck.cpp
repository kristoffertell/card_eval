// Copyright (C) 2019 Kristoffer Tell - All Rights Reserved

#include <iostream>
#include <thread>
#include <limits>
#include <unistd.h>

#include "misc.h"
#include "deck.h"

using namespace std;


Deck::Deck() :
	m_initialized(false),
	m_needs_update(true),
	m_start_selling(false),
	m_deck_updater(&Deck::start_updater, this),
	m_card_seller(&Deck::start_seller, this)
{

}


Deck::~Deck()
{
	m_deck_updater.join();
	m_card_seller.join();
}


/**
 * Initializes the deck.
 *
 * Card data is parsed and put in a vector.
 * Sets m_initialized to true if init was successful
 * in order to tell the two helper threads that it's
 * okay to proceed.
 *
 * @param data The input data
 * @return true if init was successful
 */
bool Deck::Init(vector<string> data)
{
	unsigned num_cards = 0;
	unsigned red = 0;
	unsigned green = 0;
	unsigned blue = 0;
	unsigned id = 0;

	size_t pos = 0;
	size_t pos2 = 0;
	string buf = "";

	// Parse number of cards from first line and then
	// red, green, blue and id from every line afterwards
	bool first_line = true;
	for (string line : data)
	{
		if (first_line) {
			num_cards = stoi(line, nullptr, 10);
			if (num_cards == 0) {
				cout << "error: invalid number of cards, cannot have 0" << endl;
				return false;
			} else if (num_cards > 1000) {
				cout << "error: invalid number of cards, cannot have >1000" << endl;
				return false;
			}
			first_line = false;
			continue;
		}

		// Get number before first comma
		pos = line.find(",");
		if (pos == string::npos) {
			cout << "error: invalid line, cannot find first comma: " << line << endl;
			return false;
		}
		buf = line.substr(0, pos);
		if (!is_number(buf)) {
			cout << "error: invalid line, not a number: " << buf << endl;
			return false;
		}
		red = stoi(buf, nullptr, 10);

		// Get number between first and second comma
		pos2 = line.find(",", pos+1);
		if (pos2 == string::npos) {
			cout << "error: invalid line, cannot find second comma: " << line << endl;
			return false;
		}
		buf = line.substr(pos+1, pos2-pos-1);
		if (!is_number(buf)) {
			cout << "error: invalid line, not a number: " << buf << endl;
			return false;
		}
		green = stoi(buf, nullptr, 10);

		// Get number between second and third comma
		pos = line.find(",", pos2+1);
		if (pos == string::npos) {
			cout << "error: invalid line, cannot find third comma: " << line << endl;
			return false;
		}
		buf = line.substr(pos2+1, pos-pos2-1);
		if (!is_number(buf)) {
			cout << "error: invalid line, not a number: " << buf << endl;
			return false;
		}
		blue = stoi(buf, nullptr, 10);

		// Get number after third comma
		buf = line.substr(pos+1);
		if (!is_number(buf)) {
			cout << "error: invalid line, not a number: " << buf << endl;
			return false;
		}
		id = stoi(buf, nullptr, 10);

		Card new_card;
		new_card.SetAngles(red, green, blue);
		new_card.SetId(id);
		add_card(new_card);
	}

	if (get_num_cards() != num_cards) {
		cout << "error: number of cards in data (" << get_num_cards() <<
			") does not match number specifed on first line (" <<
			num_cards << ")" << endl;
		return false;
	}

	m_initialized = true;
	return true;
}


void Deck::SellCards()
{
	m_start_selling = true;
}


bool Deck::Empty()
{
	return (get_num_cards() == 0);
}


void Deck::add_card(Card& card)
{
	m_mutex.lock();
	m_cards.push_back(card);
	m_mutex.unlock();
}


unsigned Deck::get_num_cards()
{
	int num_cards = 0;
	m_mutex.lock();
	num_cards = m_cards.size();
	m_mutex.unlock();
	return num_cards;
}


/**
 * Start the updater thread work loop.
 *
 * Will only re-evaluate the uniqueness of the cards
 * in the deck if the seller thread has told this thread
 * through the m_needs_update variable that the deck needs
 * to be updated because it has changed.
 */
void Deck::start_updater()
{
	while (!m_initialized) {
		usleep(1);
	}
	while (!Empty() && !g_shutdown) {
		while (!m_needs_update) {
			usleep(1);
		}
		update_uniq();
		m_needs_update = false;
	}
}


/**
 * Start the seller thread work loop.
 *
 * Will start selling one card at a time once the deck
 * has been initialized properly and the main thread has
 * set m_start_selling to true. Will always sell the card
 * with the lowest uniqueness score and will continue until
 * all cards are sold.
 * Sets the m_needs_update variable to true in order to
 * tell the updater thread that it should do some work.
 */
void Deck::start_seller()
{
	while (!m_initialized) {
		usleep(1);
	}
	while (!m_start_selling) {
		usleep(1);
	}
	while (!Empty() && !g_shutdown) {
		while (m_needs_update) {
			usleep(1);
		}
		sell_least_unique();
		m_needs_update = true;
	}
}


/**
 * Find closest angle BEFORE a specific cards angle.
 *
 * The deck is searched to find which angle from another card
 * comes closest before (i.e. counter-clockwise from) the angle
 * of incard with color angt.
 *
 * @param incard The card to compare angles with.
 * @param angnt Which color angle to compare.
 * @return The closest angle that was found.
 */
unsigned Deck::find_closest_before(Card& incard, Card::angle_type angt)
{
	/* With 2 cards left, only the ID decides which one
	 * will be sold and with 1 card left it's the only
	 * one that can be sold. So we just return 0 here
	 * in those cases since it doesn't matter. */
	if (get_num_cards() <= 2) {
		return 0;
	}

	unsigned limit = incard.Angle(angt);
	unsigned angle = 0;
	bool angle_found = false;

	for (Card& card : m_cards) {
		if ((card.Id() != incard.Id()) &&
			(card.Angle(angt) <= limit) &&
			(card.Angle(angt) >= angle))
		{
			angle = card.Angle(angt);
			angle_found = true;
		}
	}

	if (!angle_found) {
		angle = 0;
		limit = incard.Angle(angt);
		for (Card& card : m_cards) {
			if ((card.Id() != incard.Id()) &&
				(card.Angle(angt) >= limit) &&
				(card.Angle(angt) >= angle))
			{
				angle = card.Angle(angt);
				angle_found = true;
			}
		}
	}

	return angle;
}


/**
 * Find closest angle AFTER a specific cards angle.
 *
 * The deck is searched to find which angle from another card
 * comes closest after (i.e. clockwise from) the angle of
 * incard with color angt.
 *
 * @param incard The card to compare angles with.
 * @param angnt Which color angle to compare.
 * @return The closest angle that was found.
 */
unsigned Deck::find_closest_after(Card& incard, Card::angle_type angt)
{
	/* With 2 cards left, only the ID decides which one
	 * will be sold and with 1 card left it's the only
	 * one that can be sold. So we just return 0 here
	 * in those cases since it doesn't matter. */
	if (get_num_cards() <= 2) {
		return 0;
	}

	unsigned limit = incard.Angle(angt);
	unsigned angle = 359;
	bool angle_found = false;

	for (Card& card : m_cards) {
		if ((card.Id() != incard.Id()) &&
			(card.Angle(angt) >= limit) &&
			(card.Angle(angt) <= angle))
		{
			angle = card.Angle(angt);
			angle_found = true;
		}
	}

	if (!angle_found) {
		angle = 0;
		limit = incard.Angle(angt);
		for (Card& card : m_cards) {
			if ((card.Id() != incard.Id()) &&
				(card.Angle(angt) <= limit) &&
				(card.Angle(angt) <= angle))
			{
				angle = card.Angle(angt);
				angle_found = true;
			}
		}
	}

	return angle;
}


/**
 * Update uniqueness value of all cards in deck.
 *
 * Checks every card in deck to see which red, green and blue
 * angles come closest before and after each cards angles.
 * The total clockwise angle between the two closest angles
 * for each color is summed up to become the uniqueness value
 * for that card.
 */
void Deck::update_uniq()
{
	int uniq = 0;
	unsigned a = 0;
	unsigned b = 0;
	for (auto &card : m_cards) {
		a = find_closest_before(card, Card::angle_type::RED);
		b = find_closest_after(card, Card::angle_type::RED);
		uniq = compute_angle(a, b);

		a = find_closest_before(card, Card::angle_type::GREEN);
		b = find_closest_after(card, Card::angle_type::GREEN);
		uniq += compute_angle(a, b);

		a = find_closest_before(card, Card::angle_type::BLUE);
		b = find_closest_after(card, Card::angle_type::BLUE);
		uniq += compute_angle(a, b);

		card.SetUniq(uniq);
	}
}


/**
 * Sells the least unique card in the deck.
 *
 * Finds the card in the deck that has the lowest uniqueness value,
 * prints the ID of it and then removes the card from the deck.
 * If two cards have the same uniqueness, the card with the
 * highest ID will be the one that's sold.
 */
void Deck::sell_least_unique()
{
	int lowest_uniq = std::numeric_limits<int>::max();
	m_mutex.lock();
	auto it_chosen = m_cards.begin();
	for (auto it = m_cards.begin(); it != m_cards.end(); it++) {
		if (it->Uniq() < lowest_uniq) {
			lowest_uniq = it->Uniq();
			it_chosen = it;
		} else if ((it->Uniq() == lowest_uniq) && (it->Id() > it_chosen->Id())) {
			it_chosen = it;
		}
	}
	it_chosen->PrintId();
	m_cards.erase(it_chosen);
	m_mutex.unlock();
}
