// Copyright (C) 2019 Kristoffer Tell - All Rights Reserved

class Card
{
	public:
		Card();
		void SetAngles(unsigned red, unsigned green, unsigned blue);
		void SetId(unsigned id);
		void SetUniq(unsigned uniq);

		enum class angle_type {
			RED,
			GREEN,
			BLUE
		};

		unsigned Angle(angle_type angle);
		unsigned Id() { return m_id; }
		int Uniq() { return m_uniq; }
		void PrintId();

	protected:
		unsigned m_angle_red;
		unsigned m_angle_green;
		unsigned m_angle_blue;
		unsigned m_id;
		int m_uniq;
};
