// Copyright (C) 2019 Kristoffer Tell - All Rights Reserved

#include <iostream>

#include "card.h"

using namespace std;


Card::Card() :
	m_angle_red(0),
	m_angle_green(0),
	m_angle_blue(0),
	m_id(0),
	m_uniq(-1)
{

}


void Card::SetAngles(unsigned red, unsigned green, unsigned blue)
{
	m_angle_red = red;
	m_angle_green = green;
	m_angle_blue = blue;
}


void Card::SetId(unsigned id)
{
	m_id = id;
}


void Card::SetUniq(unsigned uniq)
{
	m_uniq = uniq;
}


unsigned Card::Angle(angle_type angle) {
	switch (angle) {
		case angle_type::RED:
			return m_angle_red;
		case angle_type::GREEN:
			return m_angle_green;
		default: //angle_type::BLUE
			return m_angle_blue;
	}
}


void Card::PrintId()
{
	cout << m_id << endl;
}
