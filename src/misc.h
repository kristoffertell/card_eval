// Copyright (C) 2019 Kristoffer Tell - All Rights Reserved

#include <string>
#include <vector>
#include <atomic>

extern std::atomic<bool> g_shutdown;
extern unsigned const min_line_length;
extern unsigned const max_line_length;

void sig_handler(const int s);
bool is_number(const std::string& s);
bool get_input(std::vector<std::string>& data, int argc, char* argv[]);
bool validate_input(std::vector<std::string>& data);
unsigned compute_angle(unsigned a, unsigned b);
