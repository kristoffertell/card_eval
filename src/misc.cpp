// Copyright (C) 2019 Kristoffer Tell - All Rights Reserved

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <atomic>
#include <signal.h>
#include <unistd.h>

#include "misc.h"

using namespace std;

atomic<bool> g_shutdown = false;
unsigned const min_line_length = 7;
unsigned const max_line_length = 15;


/**
 * Handles SIGINT sent by Ctrl-C
 */
void sig_handler(const int s)
{
	cout << "got ctrl-c" << endl;
	g_shutdown = true;
}


/**
 * Checks if string is a number
 *
 * @param s String to check
 * @return true if s is a number
 */
bool is_number(const string& s)
{
	if (s.empty()) {
		return false;
	}
	string buf = s;
	for (char& c : buf) {
		if (!isdigit(c)) {
			return false;
		}
	}
	return true;
}


/**
 * Gets input from file or stdin
 *
 * If a filename was specified as an argument, this
 * function tries to read the contents of that file.
 * Otherwise, input is read directly from cin.
 *
 * @param data A vector of strings to put the data in
 * @param argc Argument count from main
 * @param argv Argument array from main
 * @return true if data was read successfully
 */
bool get_input(vector<string>& data, int argc, char* argv[])
{
	data.clear();
	ifstream input_stream;
	if (argc == 2) { // specified file
		input_stream.open(argv[1], ifstream::in);
		if (!input_stream.is_open()) {
			cout << "error: cannot read file " << argv[1] << endl;
			return false;
		}
		for (string line; getline(input_stream, line);) {
			data.push_back(line);
		}
	} else { // piped input
		for (string line; getline(cin, line);) {
			data.push_back(line);
		}
	}
	return true;
}


/**
 * Validates input
 *
 * @param data The data to validate
 * @return true if data seems ok
 */
bool validate_input(vector<string>& data) {
	bool first_line = true;
	int num_cards = 0;
	int line_count = 0;
	for (auto const& line: data) {
		if (first_line) {
			first_line = false;
			if (!is_number(line)) {
				cout << "error: invalid number of cards: " << line << endl;
				return false;
			}
			num_cards = stoi(line, nullptr, 10);
		} else {
			line_count++;
			if ((line.length() < min_line_length) ||
				(line.length() > max_line_length)) {
				cout << "error: invalid line: " << line << endl;
				return false;
			}
			size_t pos = line.find(",");
			for (int i = 0; i < 3; i++) {
				if (pos == string::npos) {
					cout << "error: invalid line, missing values: " << line << endl;
					return false;
				}
				pos = line.find(",", pos+1);
			}
			if (pos != string::npos) {
				cout << "error: invalid line, too many values: " << line << endl;
				return false;
			}
		}
	}
	if (num_cards != line_count) {
		cout << "error: number of cards in data (" << line_count <<
			") does not match number specifed on first line (" <<
			num_cards << ")" << endl;
		return false;
	}
	return true;
}


/**
 * Calculates the angle between two points on a circle.
 *
 * The result will be the distance in degrees between two
 * points when moving clockwise from a to b.
 *
 * @param a Starting point
 * @param b Endpoint
 * @return Degrees between a and b in clockwise direction
 */
unsigned compute_angle(unsigned a, unsigned b)
{
	if (a > b) {
		return 360-a+b;
	} else {
		return b-a;
	}
}
