// Copyright (C) 2019 Kristoffer Tell - All Rights Reserved

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <signal.h>
#include <unistd.h>

#include "misc.h"
#include "deck.h"

const char g_appname[] = "card_eval";
const char g_appversion[] = "1.0.0";

using namespace std;


int main(int argc, char* argv[])
{
	struct sigaction sigIntHandler;
	sigIntHandler.sa_handler = sig_handler;
	sigemptyset(&sigIntHandler.sa_mask);
	sigIntHandler.sa_flags = 0;
	sigaction(SIGINT, &sigIntHandler, NULL);

	vector<string> data;
	if (!get_input(data, argc, argv)) {
		return 1;
	}
	if (!validate_input(data)) {
		return 1;
	}

	Deck deck;
	if (!deck.Init(data)) {
		return 1;
	}

	deck.SellCards();

	while (!deck.Empty() && !g_shutdown) {
		usleep(1);
	}

	return 0;
}
