#Compiler and Linker
CC          := $(CXX)

#The Target Binary Program
TARGET_PREFIX := card_eval

#The Directories, Source, Includes, Objects, Binary and Resources
SRCDIR      := src
INCDIR      := src
BUILDDIR    := obj
TARGETDIR   := bin
SRCEXT      := cpp
DEPEXT      := d
OBJEXT      := o

#Flags, Libraries and Includes
CFLAGS      := $(CXXFLAGS) -Wall -O3 -g -std=gnu++17
LIB         := $(LDFLAGS) -pthread
INC         := -I$(INCDIR) -I/usr/local/include -I/usr/include
INCDEP      := -I$(INCDIR)

# Generate output filename
APPVERSION_FILENAME:= src/main.cpp
VERSION:= $(shell cat $(APPVERSION_FILENAME) | grep --text "appversion\[\].*=.*\"" | awk 'BEGIN { FS = "\"" } ; { print $$2 }' | sed 's/\./_/g')
TARGET:= "$(TARGET_PREFIX)-$(VERSION)"

#---------------------------------------------------------------------------------
#DO NOT EDIT BELOW THIS LINE
#---------------------------------------------------------------------------------
SOURCES     := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS     := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.$(OBJEXT)))

#Defauilt Make
all: directories $(TARGET)

#Remake
remake: cleaner all

#Make the Directories
directories:
	@mkdir -p $(BUILDDIR)
	@mkdir -p $(TARGETDIR)

#Clean only Objecst
clean:
	@$(RM) -rf $(BUILDDIR)

#Full Clean, Objects and Binaries
cleaner: clean
	@$(RM) -rf $(TARGETDIR)

#Pull in dependency info for *existing* .o files
-include $(OBJECTS:.$(OBJEXT)=.$(DEPEXT))

#Link
$(TARGET): $(OBJECTS)
	@/bin/echo -en "\033[96m"
	@/usr/bin/printf "%-30s " "linking $<"
	@$(CC) -o $(TARGETDIR)/$(TARGET) $^ $(LIB)
	@/usr/bin/printf "OK!\n"
	@/usr/bin/printf "\n%s/%s baked and ready" $(TARGETDIR) $(TARGET)
	@/bin/echo -e "\033[0m"

#Compile
$(BUILDDIR)/%.$(OBJEXT): $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(dir $@)
	@/bin/echo -en "\033[96m"
	@/usr/bin/printf "%-30s " "compiling $<"
	@$(CC) $(CFLAGS) $(INC) -c -o $@ $<
	@$(CC) $(CFLAGS) $(INCDEP) -MM $(SRCDIR)/$*.$(SRCEXT) > $(BUILDDIR)/$*.$(DEPEXT)
	@cp -f $(BUILDDIR)/$*.$(DEPEXT) $(BUILDDIR)/$*.$(DEPEXT).tmp
	@sed -e 's|.*:|$(BUILDDIR)/$*.$(OBJEXT):|' < $(BUILDDIR)/$*.$(DEPEXT).tmp > $(BUILDDIR)/$*.$(DEPEXT)
	@sed -e 's/.*://' -e 's/\\$$//' < $(BUILDDIR)/$*.$(DEPEXT).tmp | fmt -1 | sed -e 's/^ *//' -e 's/$$/:/' >> $(BUILDDIR)/$*.$(DEPEXT)
	@rm -f $(BUILDDIR)/$*.$(DEPEXT).tmp
	@/usr/bin/printf "OK!"
	@/bin/echo -e "\033[0m"

#Non-File Targets
.PHONY: all remake clean cleaner resources
