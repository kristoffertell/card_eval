# Compass Card Evaluator
Application can be started either with an input file as a parameter or with a pipe to stdin.  

**Example 1**  
`./card_eval-1_0_0 example.cards`  

**Example 2**  
`cat example.cards | ./card_eval-1_0_0`  

**Input**  
The first line of input contains an integer n, the number of cards (1 <= n <= 1000). Then follows n lines. Each of these lines contains 4 integers r, g, b, id (0 <= r, g, b < 360, 0 <= id < 1000), giving the red, green and blue angles as well as the ID of a card. Two or more cards are not allowed to have the same ID.

**Input example**  
```
8
331,304,284,417
263,109,45,116
6,202,299,501
133,202,263,690
15,17,77,131
324,144,216,443
173,243,43,127
16,166,166,312
```


**Output**  
Output lines, containing the IDs of the cards in the order they are to be sold, from first (least unique) to last (most unique).

**Output example**  
```
501
443
417
116
690
131
312
127
```
