#!/bin/bash

# This script generates a new list of random cards
# to be used together with the card_eval app for
# testing purposes. A new card list with a unique
# filename is created every time the script is run
# and the result is placed under ./bin/
#
# Generated files will be named:
# TIMESTAMP_SUFFIX.cards
# where TIMESTAMP is the current date and time
# and SUFFIX is a random 6 digit hex value.

mkdir bin 2>/dev/null

ts="$(date +%Y%m%d%H%M%S)"
suffix="$(cat /dev/urandom | tr -cd 'a-f0-9' | head -c 6)"
fname="bin/${ts}_${suffix}.cards"

card_max=$(echo $RANDOM % 1000 | bc)
angle=$(echo $RANDOM % 360 | bc)
declare -A cards

echo "Number of cards: $((card_max+1))"
echo $((card_max+1)) >$fname

echo -n "Generating cards..."

for ((i=0; i<=$card_max; i++)); 
do
	id_exists=1
	while [ $id_exists -eq 1 ]; do
		id_exists=0
		id=$(echo $RANDOM % 1000 | bc)
		if [ ${cards[$id]} ]; then
			id_exists=1
		fi
	done
	angle1=$(echo $RANDOM % 360 | bc)
	angle2=$(echo $RANDOM % 360 | bc)
	angle3=$(echo $RANDOM % 360 | bc)
	cards[$id]="$angle1,$angle2,$angle3,$id"
	echo "${cards[$id]}" >>$fname
done

echo "...done!"
echo
echo "$fname"
echo

