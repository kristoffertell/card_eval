#!/bin/bash
appname=$(grep "g_appname" ../src/main.cpp | cut -d '"' -f2)
version=$(grep "g_appversion" ../src/main.cpp | cut -d '"' -f2 | tr . _)
binary="${appname}-${version}"
testfile=test.cards

output_ref="$(cat test.result)"
output_real="$(../bin/$binary $testfile)"
output_real2="$(cat $testfile | ../bin/$binary)"

diff <(echo "$output_ref") <(echo "$output_real") >/dev/null 2>&1
res=$?

diff <(echo "$output_ref") <(echo "$output_real2") >/dev/null 2>&1
res2=$?

if [ $res -eq 0 ] && [ $res2 -eq 0 ]; then
    echo "OK"
else
    echo "FAIL"
fi
